import React from "react";
import styles from "./assets/Tujuh.module.css";


class Tujuh extends React.Component {
   render() {
      return (
         <div className={styles.body}>
            <div className={styles.div}>1</div>
            <div className={styles.div}>2</div>
            <div className={styles.div}>3</div>
            <div className={styles.div}>4</div>
         </div>
      );
   }
};

export default Tujuh;