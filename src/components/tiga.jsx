import React from "react";
import styles from "./assets/Tiga.module.css";


class Tiga extends React.Component {
    render() {
        return (
            <div className={styles.body}>
                <div className={styles.sidebar} contenteditable>
                    Min: 150px
                    <br />
                    Max: 25%
                </div>
                <p className={styles.content} contenteditable>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis nulla nulla architecto maxime modi nisi. Quas saepe dolorum, architecto quia fugit nulla! Natus, iure eveniet ex iusto tempora animi quibusdam porro?</p>
            </div>
        );
    }
};

export default Tiga;