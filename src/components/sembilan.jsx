import React from "react";
import styles from "./assets/Sembilan.module.css";


class Sembilan extends React.Component {
  render() {
    return (
      <div className={styles.body}>
        <div className={styles.card}>
          <div className={styles.h1}>Title Here</div>
          <div className={styles.visual}></div>
          <p>Descriptive Text. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sed est error repellat veritatis.</p>
          <br />
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum eveniet beatae veritatis saepe corporis voluptates illo placeat maxime sapiente. Sit facere cumque quidem ad quo, dolores pariatur repudiandae ullam animi?</p>
        </div>
      </div>
    );
  }
};

export default Sembilan;