import React from "react";
import styles from "./assets/Sepuluh.module.css";


class Sepuluh extends React.Component {
  render() {
    return (
      <div className={styles.body}>
        <div className={styles.card}>
          <div className={styles.h1}>Title Here</div>
          <div className={styles.visual}></div>
          <p>Descriptive Text. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sed est error repellat veritatis.</p>
        </div>
      </div>
    );
  }
};

export default Sepuluh;