
import React from "react";
import styles from "./assets/Delapan.module.css";


class Delapan extends React.Component {
    render() {
        return (
            <div className={styles.body}>
                <div className={styles.card}>
                    <div className={styles.h1}>Title - Card 1</div>
                    <p>Medium length description. Let's add a few more words.</p>
                    <div className={styles.visual}></div>
                </div>
                <div className={styles.card}>
                    <div className={styles.h1}>Title - Card 2</div>
                    <p>Long Description Text. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sed est error repellat veritatis.</p>
                    <div className={styles.visual}></div>
                </div>
                <div className={styles.card}>
                    <div className={styles.h1}>Title - Card 3</div>
                    <p>Short Description.</p>
                    <div className={styles.visual}></div>
                </div>
            </div>
        );
    }
};

export default Delapan;