import React from "react";
import styles from "./assets/Satu.module.css";


class Satu extends React.Component {
   render() {
      return (
         <div className={styles.body}>
            <div className={styles.parent}>
               <div className={styles.child}>
                  #
               </div>
            </div>
         </div>
      );
   }
};

export default Satu;