import React from "react";
import styles from "./assets/Lima.module.css";


class Lima extends React.Component {
   render() {
      return (
         <div className={styles.body}>
            <div className={styles.header}><h1 contenteditable>Header</h1></div>
            <div className={styles.leftSidebar} contenteditable>Left Sidebar</div>
            <div className={styles.main} contenteditable>Main Content</div>
            <div className={styles.rightSidebar} contenteditable>Right Sidebar</div>
            <div className={styles.footer} contenteditable>Footer</div>
         </div>
      );
   }
};

export default Lima;