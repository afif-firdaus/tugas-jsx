import React from "react";
import styles from "./assets/Enam.module.css";


class Enam extends React.Component {
   render() {
      return (
         <div className={styles.body}>
            <div className={`${styles.span12} ${styles.div}`}>Span 12</div>
            <div className={`${styles.span6} ${styles.div}`}>Span 6</div>
            <div className={`${styles.span4} ${styles.div}`}>Span 4</div>
            <div className={`${styles.span2} ${styles.div}`}>Span 2</div>
         </div>
      );
   }
};

export default Enam;