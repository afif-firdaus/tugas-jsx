import Satu from "./components/satu.jsx";
import Dua from "./components/dua.jsx";
import Tiga from "./components/tiga.jsx";
import Empat from "./components/empat.jsx";
import Lima from "./components/lima.jsx";
import Enam from "./components/enam.jsx";
import Tujuh from "./components/tujuh.jsx";
import Delapan from "./components/delapan.jsx";
import Sembilan from "./components/sembilan.jsx";
import Sepuluh from "./components/sepuluh.jsx";

import React, {Component} from 'react';

function App() {
  return (
    <>
    <Satu />
    <Dua />
    <Tiga />
    <Empat />
    <Lima />
    <Enam />
    <Tujuh />
    <Delapan />
    <Sembilan />
    <Sepuluh />
    </>
  );
}

export default App;
